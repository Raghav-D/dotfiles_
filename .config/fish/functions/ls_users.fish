# Defined via `source`
function ls_users --wraps=awk\ -F\ :\ \'\{print\ \}\'\ /etc/passwd --wraps=awk\ -F\ :\ \'\{print\ \$1\}\'\ /etc/passwd --description alias\ ls_users\ awk\ -F\ :\ \'\{print\ \$1\}\'\ /etc/passwd
  awk -F : '{print $1}' /etc/passwd $argv; 
end
