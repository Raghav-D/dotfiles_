# Defined via `source`
function brave --wraps=open\ -a\ Brave\\\ Browser.app --wraps=open\ -a\ Brave\\\ Browser\\\ Beta.app --description alias\ brave\ open\ -a\ Brave\\\ Browser.app
  open -a Brave\ Browser.app $argv; 
end
