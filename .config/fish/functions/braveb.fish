# Defined via `source`
function braveb --wraps=open\ -a\ Brave\\\ Browser\\\ Beta.app --description alias\ braveb\ open\ -a\ Brave\\\ Browser\\\ Beta.app
  open -a Brave\ Browser\ Beta.app $argv; 
end
